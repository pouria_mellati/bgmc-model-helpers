package bgmc.actormodelhelpers;

import java.util.List;
import java.util.Map;

public class ActorStateSuccessorDelta {
	ActorState actorStateAfterRunningMsgHandler;
	Map<String, ActorState> newlyInstantiatedActors;
	Map<String, List<Message>> sentMessages;
	
	public ActorStateSuccessorDelta(
			ActorState actorStateAfterRunningMsgHandler,
			Map<String, ActorState> newlyInstantiatedActors,
			Map<String, List<Message>> sentMessages) {
		
		this.actorStateAfterRunningMsgHandler = actorStateAfterRunningMsgHandler;
		this.newlyInstantiatedActors = newlyInstantiatedActors;
		this.sentMessages = sentMessages;
	}

	public ActorState getActorStateAfterRunningMsgHandler() {
		return actorStateAfterRunningMsgHandler;
	}
	
	public Map<String, ActorState> getNewlyInstantiatedActors() {
		return newlyInstantiatedActors;
	}
	
	public Map<String, List<Message>> getSentMessages() {
		return sentMessages;
	}
}
