package bgmc.actormodelhelpers;

import java.util.List;

// Must be immutable, since instances will be shared.
public class Message {
	public String name;
	public List<Object> data;

	public Message(String name) {
		this.name = name;
		this.data = null;
	}
	
	// TODO: Use varargs instead of a list for data.
	public Message(String name, List<Object> data) {
		this.name = name;
		this.data = data;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Message other = (Message) obj;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}
