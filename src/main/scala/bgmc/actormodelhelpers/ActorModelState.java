package bgmc.actormodelhelpers;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.TreeMap;

import bgmc.ObjectVal;
import bgmc.ModelState;
import bgmc.actormodelhelpers.actordefinition.SimpleObjectVal;

public class ActorModelState implements ModelState {
	protected Map<String, ActorState> actorsById = null;
	protected List<ModelState> successors = null;
	protected Map<String, List<ObjectVal>> actorObjValsByClassId = null;
	protected String actorThatExecutedToReachThisState = null;
	
	protected Collection<String> activeActorIds = null;
	
	
	public ActorModelState(Map<String, ActorState> actorsById, String actorThatExecutedToReachThisState) {
		if(actorsById == null)
			throw new IllegalArgumentException("Cannot create an ActorModelState with a null actors param.");
		this.actorsById = actorsById;
		this.actorThatExecutedToReachThisState = actorThatExecutedToReachThisState;
		
		for(ActorState actorState: actorsById.values())
			ActorStatesStore.addNewOrReturnExisting(actorState);
		
		ModelStatesStore.addNewOrReturnExisting(this);
	}

	@Override
	public Collection<ModelState> getSuccessors() {
		if(successors == null) {
			successors = new LinkedList<>();
			
			for(Map.Entry<String, ActorState> actorEntry: actorsById.entrySet()) {	// For each actor in this state ...
				ActorState actorState = actorEntry.getValue();
				if(! actorState.isActive())
					continue;
				String actorId = actorEntry.getKey();
				for(ActorStateSuccessorDelta delta: actorState.getSuccessorDeltas(actorId)) {		// For each msg handling scenario of that actor ...
					Map<String, ActorState> existingActorsCopy = new HashMap<>(actorsById);
					
					Map<String, ActorState> actorsThatNeedNotBeCopiedOnWrite = delta.getNewlyInstantiatedActors();
					ActorState actorNewState = delta.getActorStateAfterRunningMsgHandler();
					actorsThatNeedNotBeCopiedOnWrite.put(actorId, actorNewState);
					
					for(Entry<String, List<Message>> messageEntry: delta.getSentMessages().entrySet()) {	// For each message sent in that scenario ...
						String recipientId = messageEntry.getKey();
						List<Message> messages = messageEntry.getValue();
						
						ActorState recipientStateAmongTheNew = actorsThatNeedNotBeCopiedOnWrite.get(recipientId);
						if(recipientStateAmongTheNew != null)
							for(Message msg: messages)
								recipientStateAmongTheNew.addMessage(msg);
						else {
							ActorState recipientAmongTheExisting = existingActorsCopy.get(recipientId);
							if(recipientAmongTheExisting == null)
								throw new RuntimeException("A message was sent to an actor that does not exist in this state.");	// TODO: Throw something better.
							ActorState recipientCopy = recipientAmongTheExisting.clone();
							for(Message msg: messages)
								recipientCopy.addMessage(msg);
							existingActorsCopy.put(recipientId, ActorStatesStore.addNewOrReturnExisting(recipientCopy));
						}
					}	// All messages have been added to their recipients.
					
					for(Map.Entry<String, ActorState> newActorEntry: actorsThatNeedNotBeCopiedOnWrite.entrySet())
						existingActorsCopy.put(newActorEntry.getKey(),
								ActorStatesStore.addNewOrReturnExisting(newActorEntry.getValue())
						);
					
					successors.add(
						ModelStatesStore.addNewOrReturnExisting(new ActorModelState(existingActorsCopy, actorId))
					);
				}	// Finished adding a successor for this message handling scenario.
			}	// Finished processing all scenarios of this member actor.
		}	// Finished initializing successors.
		return successors;
	}

	@Override
	public Collection<String> getObjectIdsByClassId(String classId) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Collection<ObjectVal> getObjectsByClassId(String classId) {
		if(actorObjValsByClassId == null) {
			actorObjValsByClassId = new TreeMap<>();
		}
		
		List<ObjectVal> objVals = actorObjValsByClassId.get(classId);
		if(objVals == null) {
			objVals = new LinkedList<>();
			for(Map.Entry<String, ActorState> idStatePair : actorsById.entrySet())
				if(idStatePair.getValue().getClassId().equals(classId))
					objVals.add(new ActorAsObjectVal(idStatePair.getKey(), idStatePair.getValue(), this));
			actorObjValsByClassId.put(classId, objVals);
		}
			
		return objVals;
	}

	@Override
	public ObjectVal getObjectById(String objectId) {
		return new ActorAsObjectVal(objectId, actorsById.get(objectId), this);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + actorsById.hashCode();
		result = prime * result	+ ((actorThatExecutedToReachThisState == null) ? 0 : actorThatExecutedToReachThisState.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		ActorModelState other = (ActorModelState) obj;
		
		if(actorsById.size() != other.actorsById.size())
			return false;
		if(! actorThatExecutedToReachThisState.equals(other.actorThatExecutedToReachThisState))
			return false;
		for(Entry<String, ActorState> entry: actorsById.entrySet())
			// We only test reference equality since we do not allow duplicate ActorStates.
			if(other.actorsById.get(entry.getKey()) != entry.getValue())
				return false;
		
		return true;
	}

	@Override
	public Collection<String> getEnabledObjectsIds() {
		if(activeActorIds == null) {
			activeActorIds = new LinkedList<>();
			for(Map.Entry<String, ActorState> actorIdStatePair: actorsById.entrySet())
				if(actorIdStatePair.getValue().isActive())
					activeActorIds.add(actorIdStatePair.getKey());
		}
		return activeActorIds;
	}

	@Override
	public String getLastExecutedObjId() {
		return actorThatExecutedToReachThisState;
	}
}

class ActorAsObjectVal implements ObjectVal {
	private ActorState actorState;
	private String actorId;
	private ActorModelState containingModelState;
	
	public ActorAsObjectVal(String actorId, ActorState actorState, ActorModelState containingModelState) {
		if(actorId.equals("TrainInstance8") && actorState == null) {
			System.out.println("Making 8");
		}
		this.actorState = actorState;
		this.actorId = actorId;
		this.containingModelState = containingModelState;
	}
	
	@Override
	public ObjectVal getField(String fieldName) {
		if("_isActive".equals(fieldName)) {
//			if(actorState.isActive())
//				System.out.println("ACTIVE");
//			else System.out.println("NOT-ACTIVE");
			return new SimpleObjectVal("Boolean", actorState.isActive());
		}
		
		if("_executed".equals(fieldName)) {
//			if(actorId.equals(containingModelState.actorThatExecutedToReachThisState))
//				System.out.println("EXECUTED");
//			else System.out.println("NOT-EXECUTED");
			return new SimpleObjectVal("Boolean", actorId.equals(containingModelState.actorThatExecutedToReachThisState));
		}
		
		String requestedActorId = actorState.tryGetActorFieldId(fieldName);
		if(requestedActorId != null)
			return new ActorAsObjectVal(requestedActorId,
					containingModelState.actorsById.get(requestedActorId),
					containingModelState);
			
		return actorState.getNonActorField(fieldName);
	}

	@Override
	public Object getValue() {
		return actorId;		// Thus, actor values them-selves only support equality checking on the property side.
	}

	@Override
	public String getClassId() {
		return actorState.getClassId();
	}

	@Override
	public String getId() {
		return actorId;
	}

	@Override
	public boolean equals(Object thatObj) {
		ActorAsObjectVal that = (ActorAsObjectVal)thatObj;
		return actorId.equals(that.actorId) && actorState.equals(that.actorState);	// TODO: What about containingModelState?
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + actorId.hashCode();
		result = prime * result	+ actorState.hashCode();
		return result;
	}
}
