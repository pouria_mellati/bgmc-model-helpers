package bgmc.actormodelhelpers.actordefinition;

import bgmc.ObjectVal;

public class SimpleObjectVal implements ObjectVal {
	private Object value;
	private String classId;

	public SimpleObjectVal(String classId, Object value) {
		this.value = value;
		this.classId = classId;
	}

	@Override
	public ObjectVal getField(String fieldName) {
		return null;
	}

	@Override
	public Object getValue() {
		return value;
	}

	@Override
	public String getClassId() {
		return classId;
	}

	@Override
	public String getId() {
		throw new UnsupportedOperationException("Attempted to get the Id of a non-actor with classId: " + classId + "."); 
	}
}