package bgmc.actormodelhelpers.actordefinition;

import java.util.ArrayDeque;
import java.util.Iterator;

import bgmc.actormodelhelpers.ActorState;
import bgmc.actormodelhelpers.Message;

/** 
 * Creating an actor by sub-classing this class is EXTREMELY error prone, since care must be taken not
 * to mutate an actor in any way (e.g. removing a message from the actor's Q is even considered mutation).
 * 
 * Only actor clones created to represent the next state of the actor after running a message handler may
 * be mutated.
 * 
 * A subclass must ensure to override the clone function if it has fields that require deep copying.
 * 
 * When implementing the actorEquals() and actorHashCode(), super.equals() and super.hashCode() should
 * NOT be called, or an infinite loop will be created.
 */
public abstract class AbstractActorState implements ActorState, Cloneable {
	// TODO: Simplify the implementation of getSuccessorDeltas, by allowing the subclass to simply implement a serve(self, msg, deltas).
	
	private int maxQLength;
	private String classId;
	protected ArrayDeque<Message> messageQ;	// TODO: A custom data-structure can be used here to share parts of the message between copies of messageQs.
	
	// The implementation of this method should not call super.equals().
	abstract protected boolean actorEquals(Object thatObj);
	
	// The implementation of this method should not call super.hashCode().
	abstract protected int actorHashCode();
	
	public AbstractActorState(int maxQLength, String classId) {
		this.maxQLength = maxQLength;
		this.classId = classId;
		messageQ = new ArrayDeque<>(this.maxQLength);
	}
	
	@Override
	public String getClassId() {
		return classId;
	}
	
	@Override
	public void addMessage(Message msg) {
		if(messageQ.size() == maxQLength)
			throw new RuntimeException("An actor of type " + getClassId() + " has exceeded its max number (" + maxQLength + ") of queue messages.");
		messageQ.add(msg);
	}
	
	@Override
	public AbstractActorState clone() {
		AbstractActorState c = null;
		try {
			c = (AbstractActorState)super.clone();
		} catch (CloneNotSupportedException e) {
			// Won't happen.
		}
		c.messageQ = messageQ.clone();
		return c;
	}

	@Override
	public boolean isActive() {
		return ! messageQ.isEmpty();
	}
	
	@Override
	public int hashCode() {		
		int result = actorHashCode();
		final int prime = 31;
		for(Message m: messageQ)
			result = prime * result + m.hashCode();
		return result;
	}
	

	@Override
	public boolean equals(Object thatObj) {
		return actorEquals(thatObj) && sameElements(messageQ, ((AbstractActorState)thatObj).messageQ);
	}
	

	private boolean sameElements(ArrayDeque<Message> messageQ1, ArrayDeque<Message> messageQ2) {
		if(messageQ1 == messageQ2)
			return true;
		
		if(messageQ1.size() != messageQ2.size())
			return false;
		
		Iterator<Message> it1 = messageQ1.iterator();
		Iterator<Message> it2 = messageQ2.iterator();
		while(it1.hasNext()) {
			if(! it1.next().equals(it2.next()))
				return false;
		}
		
		return true;
	}
}
