package bgmc.actormodelhelpers;

import java.util.HashMap;

public class ModelStatesStore {
	private static HashMap<ActorModelState, ActorModelState> store = new HashMap<>(1000);
	
	public static ActorModelState addNewOrReturnExisting(ActorModelState actorModelState) {
		ActorModelState stored = store.get(actorModelState);
		if(stored == null) {
			store.put(actorModelState, actorModelState);
			stored = actorModelState;
		}
		return stored;
	}
}
