package bgmc.actormodelhelpers;

import java.util.HashMap;

public class ActorStatesStore {
	private static HashMap<ActorState, ActorState> store = new HashMap<>(1000);
	
	public static ActorState addNewOrReturnExisting(ActorState actorState) {
		ActorState stored = store.get(actorState);
		if(stored == null) {
			store.put(actorState, actorState);
			stored = actorState;
		}
		return stored;
	}
}
