package bgmc.actormodelhelpers;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import bgmc.Model;
import bgmc.ModelState;

public class ActorModel implements Model {
	List<ModelState> initialStates = new LinkedList<>();

	public ActorModel(List<ActorModelState> initialActorModelStates) {
		if(initialActorModelStates == null || initialActorModelStates.isEmpty())
			throw new IllegalArgumentException("Cannot create an ActorModel from a null or empty list of initial states.");
		
		for(ActorModelState ms: initialActorModelStates)
			initialStates.add(ms);
	}

	@Override
	public Collection<ModelState> getInitialStates() {
		return initialStates;
	}

}
