package bgmc.actormodelhelpers;

import java.util.List;

import bgmc.ObjectVal;

public interface ActorState {
	List<ActorStateSuccessorDelta> getSuccessorDeltas(String actorId);
	void addMessage(Message msg);
	ActorState clone();
	boolean isActive();
	String getClassId();
	
	// Throw an exception if not found.
	ObjectVal getNonActorField(String fieldName);
	
	// Returns the id of the actor that is held as fieldName if such a field exists, else returns null.
	String tryGetActorFieldId(String fieldName);
}
