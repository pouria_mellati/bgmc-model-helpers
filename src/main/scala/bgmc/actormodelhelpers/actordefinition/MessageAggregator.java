package bgmc.actormodelhelpers.actordefinition;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import bgmc.actormodelhelpers.Message;

public class MessageAggregator {
	public Map<String, List<Message>> sentMessages = new HashMap<>(0);
	
	public void send(String recipientId, Message msg) {
		getExistingOrSetupMessageListForRecipient(recipientId).add(msg);
	}
	
	public void sendAll(Map<String, List<Message>> messages) {
		for(Map.Entry<String, List<Message>> recipientIdAndMessages: messages.entrySet()) {
			String recipientId = recipientIdAndMessages.getKey();
			getExistingOrSetupMessageListForRecipient(recipientId).addAll(recipientIdAndMessages.getValue());
		}
	}
	
	private List<Message> getExistingOrSetupMessageListForRecipient(String recipientId) {
		List<Message> messagesForThisRecipient = sentMessages.get(recipientId);
		if(messagesForThisRecipient == null) {
			messagesForThisRecipient = new LinkedList<>();
			sentMessages.put(recipientId, messagesForThisRecipient);
		}
		return messagesForThisRecipient;
	}
}